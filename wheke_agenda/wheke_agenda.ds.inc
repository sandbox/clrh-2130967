<?php
/**
 * @file
 * wheke_agenda.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function wheke_agenda_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|event|agenda';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'event';
  $ds_fieldsetting->view_mode = 'agenda';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h4',
        'class' => '',
      ),
    ),
  );
  $export['node|event|agenda'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|event|bloc_agenda';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'event';
  $ds_fieldsetting->view_mode = 'bloc_agenda';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|event|bloc_agenda'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|event|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'event';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|event|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|event|teaser';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'event';
  $ds_fieldsetting->view_mode = 'teaser';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|event|teaser'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function wheke_agenda_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|agenda';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'agenda';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'right' => array(
        0 => 'field_event_date',
        2 => 'title',
        3 => 'field_event_type',
        4 => 'field_ex_homebranch',
        5 => 'field_event_location',
      ),
      'left' => array(
        1 => 'field_image',
      ),
      'footer' => array(
        6 => 'group_more_details',
        7 => 'field_event_public',
        8 => 'body',
        9 => 'field_event_contact',
      ),
    ),
    'fields' => array(
      'field_event_date' => 'right',
      'field_image' => 'left',
      'title' => 'right',
      'field_event_type' => 'right',
      'field_ex_homebranch' => 'right',
      'field_event_location' => 'right',
      'group_more_details' => 'footer',
      'field_event_public' => 'footer',
      'body' => 'footer',
      'field_event_contact' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|event|agenda'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|bloc_agenda';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'bloc_agenda';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'right' => array(
        0 => 'field_event_date',
        2 => 'field_event_type',
        3 => 'title',
      ),
      'left' => array(
        1 => 'field_image',
      ),
    ),
    'fields' => array(
      'field_event_date' => 'right',
      'field_image' => 'left',
      'field_event_type' => 'right',
      'title' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|event|bloc_agenda'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title',
      ),
      'footer' => array(
        1 => 'body',
        7 => 'field_event_contact',
      ),
      'left' => array(
        2 => 'field_image',
      ),
      'right' => array(
        3 => 'field_event_type',
        4 => 'field_ex_homebranch',
        5 => 'field_event_location',
        6 => 'field_event_public',
        8 => 'field_event_date',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'body' => 'footer',
      'field_image' => 'left',
      'field_event_type' => 'right',
      'field_ex_homebranch' => 'right',
      'field_event_location' => 'right',
      'field_event_public' => 'right',
      'field_event_contact' => 'footer',
      'field_event_date' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|event|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|event|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'event';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_image',
      ),
      'right' => array(
        1 => 'title',
        2 => 'field_event_type',
      ),
      'footer' => array(
        3 => 'field_event_date',
        4 => 'field_event_public',
        5 => 'field_ex_homebranch',
        6 => 'field_event_location',
      ),
    ),
    'fields' => array(
      'field_image' => 'left',
      'title' => 'right',
      'field_event_type' => 'right',
      'field_event_date' => 'footer',
      'field_event_public' => 'footer',
      'field_ex_homebranch' => 'footer',
      'field_event_location' => 'footer',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
  );
  $export['node|event|teaser'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function wheke_agenda_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'agenda';
  $ds_view_mode->label = 'Agenda';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['agenda'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'bloc_agenda';
  $ds_view_mode->label = 'Bloc agenda';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['bloc_agenda'] = $ds_view_mode;

  return $export;
}
