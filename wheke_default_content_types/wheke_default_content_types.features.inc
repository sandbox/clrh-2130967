<?php
/**
 * @file
 * wheke_default_content_types.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wheke_default_content_types_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function wheke_default_content_types_node_info() {
  $items = array(
    'biblio' => array(
      'name' => t('Biblio'),
      'base' => 'node_content',
      'description' => t('Type de contenu pour les notices bibliographiques importées des catalogues'),
      'has_title' => '1',
      'title_label' => t('Titre'),
      'help' => '',
    ),
    'revue' => array(
      'name' => t('Revue'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
