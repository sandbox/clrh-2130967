<?php
/**
 * @file
 * wheke_agenda.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function wheke_agenda_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_more_details|node|event|agenda';
  $field_group->group_name = 'group_more_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'event';
  $field_group->mode = 'agenda';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Plus de détails',
    'weight' => '6',
    'children' => array(
      0 => 'body',
      1 => 'field_event_public',
      2 => 'field_event_contact',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Plus de détails',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $export['group_more_details|node|event|agenda'] = $field_group;

  return $export;
}
