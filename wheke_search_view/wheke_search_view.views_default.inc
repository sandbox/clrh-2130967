<?php
/**
 * @file
 * wheke_search_view.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wheke_search_view_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'advsearch';
  $view->description = 'advsearch';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_solr_advanced_search';
  $view->human_name = 'advsearch';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Recherche avancée';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Rechercher';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Réinitialiser';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Trier par';
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 1,
      'secondary_label' => 'Limiter par',
    ),
    'sort' => array(
      'bef_format' => 'default',
      'advanced' => array(
        'collapsible' => 0,
        'collapsible_label' => 'Sort options',
        'combine' => 1,
        'combine_rewrite' => '',
        'reset' => 0,
        'reset_label' => '',
        'is_secondary' => 0,
      ),
    ),
    'search_api_views_fulltext' => array(
      'more_options' => array(
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
    'advanced_fulltext_search_fulltext_1' => array(
      'more_options' => array(
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
    'field_coded_pubdate' => array(
      'more_options' => array(
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
    'field_coded_pubdate_1' => array(
      'more_options' => array(
        'is_secondary' => 0,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
    'field_document_type' => array(
      'bef_format' => 'bef',
      'more_options' => array(
        'bef_select_all_none' => 1,
        'bef_collapsible' => 0,
        'is_secondary' => 1,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
    'field_language' => array(
      'more_options' => array(
        'is_secondary' => 1,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
    'field_ex_homebranch' => array(
      'bef_format' => 'bef',
      'more_options' => array(
        'bef_select_all_none' => 1,
        'bef_collapsible' => 0,
        'is_secondary' => 1,
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
      ),
    ),
  );
  $handler->display->display_options['exposed_form']['options']['input_required'] = 1;
  $handler->display->display_options['exposed_form']['options']['text_input_required'] = 'Sélectionner un ou plusieurs critères et cliquer Rechercher pour lancer la rechercher';
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« premier';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ précédent';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'suivant ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = 'dernier »';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Entête: Global: Result summary */
  $handler->display->display_options['header']['result']['id'] = 'result';
  $handler->display->display_options['header']['result']['table'] = 'views';
  $handler->display->display_options['header']['result']['field'] = 'result';
  $handler->display->display_options['header']['result']['content'] = 'Votre recherche a retourné @total résultats';
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'Votre recherche n\'a pas produit de résultats.

Vous pouvez consulter XXX, faire une suggestion YYY';
  /* Champ: Indexed Nœud: Bulk operations */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'search_api_index_solr_advanced_search';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['label'] = '';
  $handler->display->display_options['fields']['views_bulk_operations']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '1';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_result'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::flag_node_action:db426e51c0f7eea7fe49886ba793e3e3' => array(
      'selected' => 1,
      'use_queue' => 0,
      'skip_confirmation' => 1,
      'override_label' => 0,
      'label' => '',
    ),
    'action::system_message_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_assign_owner_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_delete_item' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_unpublish_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_unpublish_by_keyword_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_save_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::system_send_email_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_script_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::flag_node_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::views_bulk_operations_modify_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'show_all_tokens' => 1,
        'display_values' => array(
          '_all_' => '_all_',
        ),
      ),
    ),
    'action::views_bulk_operations_argument_selector_action' => array(
      'selected' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
      'settings' => array(
        'url' => '',
      ),
    ),
    'action::node_promote_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_publish_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::system_goto_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_unpromote_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_make_unsticky_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::flag_node_action:5f178df8de3872d2f2e29851c72dbb91' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
    'action::node_make_sticky_action' => array(
      'selected' => 0,
      'use_queue' => 0,
      'skip_confirmation' => 0,
      'override_label' => 0,
      'label' => '',
    ),
  );
  /* Champ: Contenu: Rendered Nœud */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['label'] = '';
  $handler->display->display_options['fields']['rendered_entity']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  $handler->display->display_options['fields']['rendered_entity']['display'] = 'view';
  $handler->display->display_options['fields']['rendered_entity']['view_mode'] = 'search_result';
  /* Sort criterion: Rechercher: Relevance */
  $handler->display->display_options['sorts']['search_api_relevance']['id'] = 'search_api_relevance';
  $handler->display->display_options['sorts']['search_api_relevance']['table'] = 'search_api_index_solr_advanced_search';
  $handler->display->display_options['sorts']['search_api_relevance']['field'] = 'search_api_relevance';
  $handler->display->display_options['sorts']['search_api_relevance']['order'] = 'DESC';
  $handler->display->display_options['sorts']['search_api_relevance']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['search_api_relevance']['expose']['label'] = 'Pertinence';
  /* Sort criterion: Indexed Nœud: Titre_tri */
  $handler->display->display_options['sorts']['search_api_aggregation_3']['id'] = 'search_api_aggregation_3';
  $handler->display->display_options['sorts']['search_api_aggregation_3']['table'] = 'search_api_index_solr_advanced_search';
  $handler->display->display_options['sorts']['search_api_aggregation_3']['field'] = 'search_api_aggregation_3';
  $handler->display->display_options['sorts']['search_api_aggregation_3']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['search_api_aggregation_3']['expose']['label'] = 'Titre';
  /* Sort criterion: Indexed Nœud: Date de publication (date) */
  $handler->display->display_options['sorts']['field_coded_pubdate']['id'] = 'field_coded_pubdate';
  $handler->display->display_options['sorts']['field_coded_pubdate']['table'] = 'search_api_index_solr_advanced_search';
  $handler->display->display_options['sorts']['field_coded_pubdate']['field'] = 'field_coded_pubdate';
  /* Sort criterion: Indexed Nœud: Auteur_tri */
  $handler->display->display_options['sorts']['search_api_aggregation_4']['id'] = 'search_api_aggregation_4';
  $handler->display->display_options['sorts']['search_api_aggregation_4']['table'] = 'search_api_index_solr_advanced_search';
  $handler->display->display_options['sorts']['search_api_aggregation_4']['field'] = 'search_api_aggregation_4';
  $handler->display->display_options['sorts']['search_api_aggregation_4']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['search_api_aggregation_4']['expose']['label'] = 'Auteur';
  /* Filter criterion: Indexed Nœud: Type de contenu */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'search_api_index_solr_advanced_search';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'biblio' => 'biblio',
    'revue' => 'revue',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Rechercher: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_solr_advanced_search';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['group'] = 1;
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Tous mots';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['search_api_views_fulltext']['fields'] = array(
    'field_subject:title' => 'field_subject:title',
  );
  /* Filter criterion: Rechercher: Advanced fulltext search */
  $handler->display->display_options['filters']['advanced_fulltext_search_fulltext_1']['id'] = 'advanced_fulltext_search_fulltext_1';
  $handler->display->display_options['filters']['advanced_fulltext_search_fulltext_1']['table'] = 'search_api_index_solr_advanced_search';
  $handler->display->display_options['filters']['advanced_fulltext_search_fulltext_1']['field'] = 'advanced_fulltext_search_fulltext';
  $handler->display->display_options['filters']['advanced_fulltext_search_fulltext_1']['value'] = array(
    'advanced_fulltext_search_fulltext_1' => array(
      0 => array(
        'field' => NULL,
        'value' => NULL,
      ),
    ),
  );
  $handler->display->display_options['filters']['advanced_fulltext_search_fulltext_1']['group'] = 1;
  $handler->display->display_options['filters']['advanced_fulltext_search_fulltext_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['advanced_fulltext_search_fulltext_1']['expose']['operator_id'] = 'advanced_fulltext_search_fulltext_1_op';
  $handler->display->display_options['filters']['advanced_fulltext_search_fulltext_1']['expose']['operator'] = 'advanced_fulltext_search_fulltext_1_op';
  $handler->display->display_options['filters']['advanced_fulltext_search_fulltext_1']['expose']['identifier'] = 'advanced_fulltext_search_fulltext_1';
  $handler->display->display_options['filters']['advanced_fulltext_search_fulltext_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['advanced_fulltext_search_fulltext_1']['mode'] = 'filter';
  /* Filter criterion: Année de */
  $handler->display->display_options['filters']['field_coded_pubdate']['id'] = 'field_coded_pubdate';
  $handler->display->display_options['filters']['field_coded_pubdate']['table'] = 'search_api_index_solr_advanced_search';
  $handler->display->display_options['filters']['field_coded_pubdate']['field'] = 'field_coded_pubdate';
  $handler->display->display_options['filters']['field_coded_pubdate']['ui_name'] = 'Année de';
  $handler->display->display_options['filters']['field_coded_pubdate']['operator'] = '>=';
  $handler->display->display_options['filters']['field_coded_pubdate']['group'] = 1;
  $handler->display->display_options['filters']['field_coded_pubdate']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_coded_pubdate']['expose']['operator_id'] = 'field_coded_pubdate_op';
  $handler->display->display_options['filters']['field_coded_pubdate']['expose']['label'] = 'Année de';
  $handler->display->display_options['filters']['field_coded_pubdate']['expose']['operator'] = 'field_coded_pubdate_op';
  $handler->display->display_options['filters']['field_coded_pubdate']['expose']['identifier'] = 'field_coded_pubdate';
  $handler->display->display_options['filters']['field_coded_pubdate']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  /* Filter criterion: Année à */
  $handler->display->display_options['filters']['field_coded_pubdate_1']['id'] = 'field_coded_pubdate_1';
  $handler->display->display_options['filters']['field_coded_pubdate_1']['table'] = 'search_api_index_solr_advanced_search';
  $handler->display->display_options['filters']['field_coded_pubdate_1']['field'] = 'field_coded_pubdate';
  $handler->display->display_options['filters']['field_coded_pubdate_1']['ui_name'] = 'Année à';
  $handler->display->display_options['filters']['field_coded_pubdate_1']['operator'] = '<=';
  $handler->display->display_options['filters']['field_coded_pubdate_1']['group'] = 1;
  $handler->display->display_options['filters']['field_coded_pubdate_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_coded_pubdate_1']['expose']['operator_id'] = 'field_coded_pubdate_1_op';
  $handler->display->display_options['filters']['field_coded_pubdate_1']['expose']['label'] = 'à';
  $handler->display->display_options['filters']['field_coded_pubdate_1']['expose']['operator'] = 'field_coded_pubdate_1_op';
  $handler->display->display_options['filters']['field_coded_pubdate_1']['expose']['identifier'] = 'field_coded_pubdate_1';
  $handler->display->display_options['filters']['field_coded_pubdate_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  /* Filter criterion: Indexed Nœud: Type de document */
  $handler->display->display_options['filters']['field_document_type']['id'] = 'field_document_type';
  $handler->display->display_options['filters']['field_document_type']['table'] = 'search_api_index_solr_advanced_search';
  $handler->display->display_options['filters']['field_document_type']['field'] = 'field_document_type';
  $handler->display->display_options['filters']['field_document_type']['value'] = array();
  $handler->display->display_options['filters']['field_document_type']['group'] = 1;
  $handler->display->display_options['filters']['field_document_type']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_document_type']['expose']['operator_id'] = 'field_document_type_op';
  $handler->display->display_options['filters']['field_document_type']['expose']['label'] = 'Type de document';
  $handler->display->display_options['filters']['field_document_type']['expose']['operator'] = 'field_document_type_op';
  $handler->display->display_options['filters']['field_document_type']['expose']['identifier'] = 'field_document_type';
  $handler->display->display_options['filters']['field_document_type']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_document_type']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_document_type']['expose']['reduce'] = 0;
  /* Filter criterion: Indexed Nœud: Langue du document */
  $handler->display->display_options['filters']['field_language']['id'] = 'field_language';
  $handler->display->display_options['filters']['field_language']['table'] = 'search_api_index_solr_advanced_search';
  $handler->display->display_options['filters']['field_language']['field'] = 'field_language';
  $handler->display->display_options['filters']['field_language']['value'] = array();
  $handler->display->display_options['filters']['field_language']['group'] = 1;
  $handler->display->display_options['filters']['field_language']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_language']['expose']['operator_id'] = 'field_language_op';
  $handler->display->display_options['filters']['field_language']['expose']['label'] = 'Langue du document';
  $handler->display->display_options['filters']['field_language']['expose']['operator'] = 'field_language_op';
  $handler->display->display_options['filters']['field_language']['expose']['identifier'] = 'field_language';
  $handler->display->display_options['filters']['field_language']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['field_language']['expose']['reduce'] = 0;
  /* Filter criterion: Indexed Nœud: Exemplaire homebranch */
  $handler->display->display_options['filters']['field_ex_homebranch']['id'] = 'field_ex_homebranch';
  $handler->display->display_options['filters']['field_ex_homebranch']['table'] = 'search_api_index_solr_advanced_search';
  $handler->display->display_options['filters']['field_ex_homebranch']['field'] = 'field_ex_homebranch';
  $handler->display->display_options['filters']['field_ex_homebranch']['value'] = array();
  $handler->display->display_options['filters']['field_ex_homebranch']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_ex_homebranch']['expose']['operator_id'] = 'field_ex_homebranch_op';
  $handler->display->display_options['filters']['field_ex_homebranch']['expose']['label'] = 'Bibliothèque';
  $handler->display->display_options['filters']['field_ex_homebranch']['expose']['operator'] = 'field_ex_homebranch_op';
  $handler->display->display_options['filters']['field_ex_homebranch']['expose']['identifier'] = 'field_ex_homebranch';
  $handler->display->display_options['filters']['field_ex_homebranch']['expose']['multiple'] = TRUE;
  $handler->display->display_options['filters']['field_ex_homebranch']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['field_ex_homebranch']['expose']['reduce'] = 0;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'advsearch';
  $handler->display->display_options['menu']['title'] = 'Recherche avancée';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $translatables['advsearch'] = array(
    t('Master'),
    t('Recherche avancée'),
    t('more'),
    t('Rechercher'),
    t('Réinitialiser'),
    t('Trier par'),
    t('Asc'),
    t('Desc'),
    t('Sélectionner un ou plusieurs critères et cliquer Rechercher pour lancer la rechercher'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« premier'),
    t('‹ précédent'),
    t('suivant ›'),
    t('dernier »'),
    t('Votre recherche a retourné @total résultats'),
    t('Votre recherche n\'a pas produit de résultats.

Vous pouvez consulter XXX, faire une suggestion YYY'),
    t('Pertinence'),
    t('Titre'),
    t('Auteur'),
    t('Tous mots'),
    t('Année de'),
    t('à'),
    t('Type de document'),
    t('Langue du document'),
    t('Bibliothèque'),
    t('Page'),
  );
  $export['advsearch'] = $view;

  return $export;
}
