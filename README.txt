Biblibre cart and view
----------------------

Purpose
-------
Provides:
 - a flag (named panier) for flgging article, page and biblio,
 - a views for the flag with export link.

installation
---------------------
Enable the module and its dependencies.

install tcpdf fpdi library:
  - follow instruction in views_pdf/README.txt,
  - tcpdf dl: http://downloads.sourceforge.net/project/tcpdf/tcpdf_5_9_207.zip,
  - fpdi dl: http://www.setasign.de/supra/kon2_dl/39034/FPDI-1.4.2.zip,

Also, download fpdf_tpl.php at http://www.setasign.de/supra/kon2_dl/30471/FPDF_TPL-1.2.zip and put the file in 
'sites/all/libraries/fpdi'
