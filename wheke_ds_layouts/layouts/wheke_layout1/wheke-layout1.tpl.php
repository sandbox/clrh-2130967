<?php
/**
 * @file
 * Template file for DS Layout 'wheke_layout1'.
 */
?>

<div class="<?php print $classes;?> clearfix">
  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <div class="wheke-layout1-region wheke-layout1-region-left <?php print $left_classes; ?>">
    <?php print $left; ?>
  </div><!-- remove white space

  --><div class="wheke-layout1-region wheke-layout1-region-main">

    <div class="wheke-layout1-region wheke-layout1-region-main-left <?php print $main_left_classes; ?>">
      <?php print $main_left; ?>
    </div><!-- remove white space

    --><div class="wheke-layout1-region wheke-layout1-region-main-right <?php print $main_right_classes; ?>">
      <?php print $main_right; ?>
    </div>

    <?php if (isset($main_bottom)): ?>
      <div class="wheke-layout1-region wheke-layout1-region-main-bottom <?php print $main_bottom_classes; ?>">
        <?php print $main_bottom; ?>
      </div>
    <?php endif; ?>

  </div>

  <?php if (isset($bottom)): ?>
    <div class="wheke-layout1-region wheke-layout1-region-bottom <?php print $bottom_classes; ?>">
      <?php print $bottom; ?>
    </div>
  <?php endif; ?>
</div>
