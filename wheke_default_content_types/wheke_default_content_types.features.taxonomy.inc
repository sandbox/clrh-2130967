<?php
/**
 * @file
 * wheke_default_content_types.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function wheke_default_content_types_taxonomy_default_vocabularies() {
  return array(
    'document_type' => array(
      'name' => 'Types de document',
      'machine_name' => 'document_type',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'itype' => array(
      'name' => 'Itype',
      'machine_name' => 'itype',
      'description' => 'itype exemplaire',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'language' => array(
      'name' => 'Langues',
      'machine_name' => 'language',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'library' => array(
      'name' => 'Bibliothèques',
      'machine_name' => 'library',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'location' => array(
      'name' => 'Localisations',
      'machine_name' => 'location',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'nouveaute' => array(
      'name' => 'Nouveautés',
      'machine_name' => 'nouveaute',
      'description' => 'new',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
