<?php
/**
 * @file
 * wheke_opacserver.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wheke_opacserver_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_opac_features_default_opac_server().
 */
function wheke_opacserver_opac_features_default_opac_server() {
$servers = array();
  $servers['opacserver'] = array(
  'serv_id' => 'opacserver',
  'serv_name' => 'opacserver',
  'serv_host' => 'http://catalogue.kohact.dev',
  'serv_connector' => 'Koha',
  'first_bib_num' => '1',
  'last_bib_num' => '100000',
  'serv_enabled' => '1',
  'status' => '1',
  'module' => NULL,
);
return $servers;
}
