<?php

/**
 * @file
 * Drush commands for Feeds OAI-PMH module.
 */

/**
 * Implements hook_drush_command().
 */
function feeds_oai_pmh_drush_command() {
  $items = array();

  $items['feeds-oai-pmh-import'] = array(
    'description' => "Import a feed (OAI-PMH only)",
    'arguments' => array(
      'feeds-name' => 'The name of the feeds importer that will be refeshed? Mandatory.',
    ),
    'options' => array(
      'nid' => 'The nid of the feeds importer that will be refreshed. Optional',
      'set' => 'The OAI-PMH Set. Optional',
      'from' => 'Retrieve only records where date >= from. Must be in ISO format (YYYY-MM-DD).',
      'until' => 'Retrieve only records where date <= until. Must be in ISO format (YYYY-MM-DD).',
    ),
    'examples' => array(
      'drush feeds-oai-pmh-import FEED_NAME',
      'drush feeds-oai-pmh-import FEED_NAME --nid=3 --set=FULLTEXT --from=2013-01-01 --until=2013-12-31',
    ),
  );

  return $items;
}

/**
 * Replacement of drupal_http_request.
 *
 * This is used to set a greater default timeout (300 seconds instead of 30).
 */
function drush_feeds_oai_pmh_http_request($url, array $options = array()) {
  $options += array('timeout' => 300.0);

  // We need to delete this variable, otherwise it will loop indefinitely.
  variable_del('drupal_http_request_function');

  $result = drupal_http_request($url, $options);

  // Reset the variable so next call to drupal_http_request will call again this
  // function.
  variable_set('drupal_http_request_function', 'drush_feeds_oai_pmh_http_request');

  return $result;
}

/**
 * Callback for drush command feeds-oai-pmh-import.
 */
function drush_feeds_oai_pmh_import($feed_name) {
  if (!isset($feed_name) || $feed_name == "") {
    drush_print(dt('The importer feed_name is required.'));
    return FALSE;
  }

  if (!($feed_nid = drush_get_option('nid'))) {
    $feed_nid = 0;
  }

  $set = drush_get_option('set');
  $from = drush_get_option('from');
  $until = drush_get_option('until');

  // Override drupal_http_request.
  $drupal_http_request_function = variable_get('drupal_http_request_function', FALSE);
  variable_set('drupal_http_request_function', 'drush_feeds_oai_pmh_http_request');

  // Set up FeedsOAIHTTPFetcher configuration.
  $feeds_source = feeds_source($feed_name, $feed_nid);
  $oldconfig = $config = $feeds_source->getConfig();

  $config['FeedsOAIHTTPFetcher']['use_dates'] = 1;

  $from = isset($from) ? $from : '1970-01-01';
  list($year, $month, $day) = explode('-', $from);
  $config['FeedsOAIHTTPFetcher']['dates']['from']['year'] = $year;
  $config['FeedsOAIHTTPFetcher']['dates']['from']['month'] = $month;
  $config['FeedsOAIHTTPFetcher']['dates']['from']['day'] = $day;

  $until = isset($until) ? $until : date('Y-m-d');
  $config['FeedsOAIHTTPFetcher']['use_dates'] = 1;
  list($year, $month, $day) = explode('-', $until);
  $config['FeedsOAIHTTPFetcher']['dates']['to']['year'] = $year;
  $config['FeedsOAIHTTPFetcher']['dates']['to']['month'] = $month;
  $config['FeedsOAIHTTPFetcher']['dates']['to']['day'] = $day;

  if (isset($set)) {
    $sets = explode(',', $set);
  }
  else {
    $sets = array($config['FeedsOAIHTTPFetcher']['set']);
  }

  foreach ($sets as $s) {
    $config['FeedsOAIHTTPFetcher']['set'] = $s;
    $feeds_source->setConfig($config);
    $feeds_source->save();

    // Feeds OAI-PMH module works in a way that force us to call import()
    // repeatedly until there is no resumptionToken.
    $source = $config['FeedsOAIHTTPFetcher']['source'];
    $variable_name = "feeds_oai:resumptionToken:$s:$source";
    $i = 1;
    do {
      print "Importing records from set $s, from $from until $until (pass $i)\n";
      $feeds_source->import();
      $resumption_token = variable_get($variable_name, '');
      $i++;
    } while ($resumption_token != '');
  }

  // Reset to old configuration.
  $feeds_source->setConfig($oldconfig);
  $feeds_source->save();

  // Cancel drupal_http_request override.
  if ($drupal_http_request_function === FALSE) {
    variable_del('drupal_http_request_function');
  }
  else {
    variable_set('drupal_http_request_function', $drupal_http_request_function);
  }
}
