<?php
/**
 * @file
 * wheke_agenda.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wheke_agenda_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wheke_agenda_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wheke_agenda_node_info() {
  $items = array(
    'event' => array(
      'name' => t('Événement'),
      'base' => 'node_content',
      'description' => t('Pour les événements qui seront ajoutés au calendrier et visibles dans l\'agenda'),
      'has_title' => '1',
      'title_label' => t('Nom de l\'événement'),
      'help' => '',
    ),
  );
  return $items;
}
