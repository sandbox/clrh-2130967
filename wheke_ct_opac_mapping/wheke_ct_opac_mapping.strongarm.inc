<?php
/**
 * @file
 * wheke_ct_opac_mapping.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function wheke_ct_opac_mapping_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'opac_record_id_field';
  $strongarm->value = 'field_biblio_identifier';
  $export['opac_record_id_field'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'opac_server_id_field';
  $strongarm->value = 'field_server_identifier';
  $export['opac_server_id_field'] = $strongarm;

  return $export;
}
