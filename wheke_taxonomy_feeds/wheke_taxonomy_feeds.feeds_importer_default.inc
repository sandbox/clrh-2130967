<?php
/**
 * @file
 * wheke_taxonomy_feeds.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function wheke_taxonomy_feeds_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'import_taxonomie';
  $feeds_importer->config = array(
    'name' => 'Import_taxonomie',
    'description' => 'Importer des csv dans les taxonomies. Attention modifier le nom de la taxonomie ! fichier csv : CODE,NOM',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ';',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsTermProcessor',
      'config' => array(
        'vocabulary' => 'language',
        'mappings' => array(
          0 => array(
            'source' => 'NOM',
            'target' => 'name',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'CODE',
            'target' => 'field_code',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['import_taxonomie'] = $feeds_importer;

  return $export;
}
