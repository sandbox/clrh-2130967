<?php
/**
 * @file
 * Template file for DS Layout 'wheke_layout2'.
 */
?>

<div class="<?php print $classes;?> clearfix">
  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <div class="wheke-layout2-region wheke-layout2-region-left <?php print $left_classes; ?>">
    <?php print $left; ?>
  </div><!-- remove white space

  --><div class="wheke-layout2-region wheke-layout2-region-right">
    <?php print $right; ?>
  </div>

  <?php if (isset($bottom)): ?>
    <div class="wheke-layout2-region wheke-layout2-region-bottom <?php print $bottom_classes; ?>">
      <?php print $bottom; ?>
    </div>
  <?php endif; ?>
</div>
