<?php

/**
 * @file
 * Install/Uninstall webmaster role and permissions.
 */

/**
 * Returns the list of permissions to grant to the role.
 */
function wheke_role_webmaster_permissions() {
  $permissions_modules = user_permission_get_modules();
  $permissions = array(
    'access content overview',
    'administer taxonomy',
    'create article content',
    'create blog content',
    'create page content',
    'delete any article content',
    'delete any blog content',
    'delete own article content',
    'delete own blog content',
    'delete own page content',
    'delete revisions',
    'edit any article content',
    'edit any blog content',
    'edit any page content',
    'edit own article content',
    'edit own blog content',
    'edit own comments',
    'edit own page content',
    'revert revisions',
    'skip comment approval',
    'use text format full_html',
    'view own unpublished content',
    'view revisions',
  );

  return array_intersect_key(drupal_map_assoc($permissions), $permissions_modules);
}

/**
 * Implements hook_install().
 */
function wheke_role_webmaster_install() {
  $role = new stdClass();
  $role->name = 'webmaster';
  $role->weight = 3;
  $status = user_role_save($role);
  if ($status) {
    $permissions = wheke_role_webmaster_permissions();
    user_role_grant_permissions($role->rid, $permissions);
  }
}

/**
 * Implements hook_uninstall().
 */
function wheke_role_webmaster_uninstall() {
  user_role_delete('webmaster');
}
