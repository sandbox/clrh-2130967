<?php
/**
 * @file
 * wheke_authority_opac_mapping.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wheke_authority_opac_mapping_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_opac_authorities_features_default_mapping().
 */
function wheke_authority_opac_mapping_opac_authorities_features_default_mapping() {
  $mappings = array();
  $mappings[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'corporate_name',
    'authority_field_name' => 'field_authtype_code',
    'authority_field_label' => 'Authtype_code',
    'mapped_with' => 'auth_type',
    'vid' => NULL,
    'vocabulary_machine_name' => NULL,
    'vocabulary_name' => NULL,
    'term_field' => NULL,
    'term_field_label' => NULL,
    'nomatch_rule' => NULL,
  );
  $mappings[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'corporate_name',
    'authority_field_name' => 'field_see',
    'authority_field_label' => 'Forme rejetée',
    'mapped_with' => 'corporate_see',
    'vid' => NULL,
    'vocabulary_machine_name' => NULL,
    'vocabulary_name' => NULL,
    'term_field' => NULL,
    'term_field_label' => NULL,
    'nomatch_rule' => NULL,
  );
  $mappings[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'corporate_name',
    'authority_field_name' => 'field_see_also',
    'authority_field_label' => 'Forme associée',
    'mapped_with' => 'corporate_see_also',
    'vid' => NULL,
    'vocabulary_machine_name' => NULL,
    'vocabulary_name' => NULL,
    'term_field' => NULL,
    'term_field_label' => NULL,
    'nomatch_rule' => NULL,
  );
  $mappings[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'corporate_name',
    'authority_field_name' => 'title',
    'authority_field_label' => 'Authority title',
    'mapped_with' => 'corporate_main_heading',
    'vid' => NULL,
    'vocabulary_machine_name' => NULL,
    'vocabulary_name' => NULL,
    'term_field' => NULL,
    'term_field_label' => NULL,
    'nomatch_rule' => NULL,
  );
  $mappings[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'personal_name',
    'authority_field_name' => 'field_see',
    'authority_field_label' => 'Forme rejetée',
    'mapped_with' => 'personal_see',
    'vid' => NULL,
    'vocabulary_machine_name' => NULL,
    'vocabulary_name' => NULL,
    'term_field' => NULL,
    'term_field_label' => NULL,
    'nomatch_rule' => NULL,
  );
  $mappings[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'personal_name',
    'authority_field_name' => 'field_see_also',
    'authority_field_label' => 'Forme associée',
    'mapped_with' => 'personal_see_also',
    'vid' => NULL,
    'vocabulary_machine_name' => NULL,
    'vocabulary_name' => NULL,
    'term_field' => NULL,
    'term_field_label' => NULL,
    'nomatch_rule' => NULL,
  );
  $mappings[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'personal_name',
    'authority_field_name' => 'title',
    'authority_field_label' => 'Authority title',
    'mapped_with' => 'personal_main_heading',
    'vid' => NULL,
    'vocabulary_machine_name' => NULL,
    'vocabulary_name' => NULL,
    'term_field' => NULL,
    'term_field_label' => NULL,
    'nomatch_rule' => NULL,
  );
  $mappings[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'subject',
    'authority_field_name' => 'field_see',
    'authority_field_label' => 'Forme rejetée',
    'mapped_with' => 'subject_see',
    'vid' => NULL,
    'vocabulary_machine_name' => NULL,
    'vocabulary_name' => NULL,
    'term_field' => NULL,
    'term_field_label' => NULL,
    'nomatch_rule' => NULL,
  );
  $mappings[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'subject',
    'authority_field_name' => 'field_see_also',
    'authority_field_label' => 'Forme associée',
    'mapped_with' => 'subject_see_also',
    'vid' => NULL,
    'vocabulary_machine_name' => NULL,
    'vocabulary_name' => NULL,
    'term_field' => NULL,
    'term_field_label' => NULL,
    'nomatch_rule' => NULL,
  );
  $mappings[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'subject',
    'authority_field_name' => 'title',
    'authority_field_label' => 'Authority title',
    'mapped_with' => 'subject_main_heading',
    'vid' => NULL,
    'vocabulary_machine_name' => NULL,
    'vocabulary_name' => NULL,
    'term_field' => NULL,
    'term_field_label' => NULL,
    'nomatch_rule' => NULL,
  );
  $mappings[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'subject_geo',
    'authority_field_name' => 'field_see',
    'authority_field_label' => 'Forme rejetée',
    'mapped_with' => 'geo_subject_see',
    'vid' => NULL,
    'vocabulary_machine_name' => NULL,
    'vocabulary_name' => NULL,
    'term_field' => NULL,
    'term_field_label' => NULL,
    'nomatch_rule' => NULL,
  );
  $mappings[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'subject_geo',
    'authority_field_name' => 'field_see_also',
    'authority_field_label' => 'Forme associée',
    'mapped_with' => 'geo_subject_see_also',
    'vid' => NULL,
    'vocabulary_machine_name' => NULL,
    'vocabulary_name' => NULL,
    'term_field' => NULL,
    'term_field_label' => NULL,
    'nomatch_rule' => NULL,
  );
  $mappings[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'subject_geo',
    'authority_field_name' => 'title',
    'authority_field_label' => 'Authority title',
    'mapped_with' => 'geo_subject_main_heading',
    'vid' => NULL,
    'vocabulary_machine_name' => NULL,
    'vocabulary_name' => NULL,
    'term_field' => NULL,
    'term_field_label' => NULL,
    'nomatch_rule' => NULL,
  );
  return $mappings;
}

/**
 * Implements hook_opac_authorities_features_default_server_type().
 */
function wheke_authority_opac_mapping_opac_authorities_features_default_server_type() {
  $server_types = array();
  $server_types[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'corporate_name',
    'matching_field' => 'auth_type',
    'value' => '/CO/',
    'weight' => '0',
  );
  $server_types[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'personal_name',
    'matching_field' => 'auth_type',
    'value' => '/NP/',
    'weight' => '0',
  );
  $server_types[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'subject',
    'matching_field' => 'auth_type',
    'value' => '/SNC/',
    'weight' => '0',
  );
  $server_types[] = array(
    'serv_id' => 'opacserver',
    'authtype' => 'subject_geo',
    'matching_field' => 'auth_type',
    'value' => '/SNG/',
    'weight' => '0',
  );
  return $server_types;
}
