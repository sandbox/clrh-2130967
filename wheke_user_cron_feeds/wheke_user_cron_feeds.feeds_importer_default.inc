<?php
/**
 * @file
 * wheke_user_cron_feeds.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function wheke_user_cron_feeds_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'import_users_cron';
  $feeds_importer->config = array(
    'name' => 'Import_users_cron',
    'description' => 'Profil feeds pour importer un csv utilisateur sans en-tête généré en cron dans Koha',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 1,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsUserProcessor',
      'config' => array(
        'roles' => array(
          3 => 0,
          4 => 0,
          5 => 0,
        ),
        'status' => '1',
        'defuse_mail' => 0,
        'mappings' => array(
          0 => array(
            'source' => '0',
            'target' => 'field_uname',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => '1',
            'target' => 'field_ufirstname',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => '2',
            'target' => 'name',
            'unique' => 1,
          ),
          3 => array(
            'source' => '3',
            'target' => 'pass',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => '4',
            'target' => 'mail',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => '5',
            'target' => 'field_ils_server',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => 1,
        'input_format' => 'plain_text',
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['import_users_cron'] = $feeds_importer;

  return $export;
}
