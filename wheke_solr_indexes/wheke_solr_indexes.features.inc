<?php
/**
 * @file
 * wheke_solr_indexes.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wheke_solr_indexes_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "facetapi" && $api == "facetapi_defaults") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_default_search_api_index().
 */
function wheke_solr_indexes_default_search_api_index() {
  $items = array();
  $items['solr_advanced_search'] = entity_import('search_api_index', '{
    "name" : "solr_advanced_search",
    "machine_name" : "solr_advanced_search",
    "description" : null,
    "server" : "solr_local",
    "item_type" : "node",
    "options" : {
      "index_directly" : 0,
      "cron_limit" : "50",
      "fields" : {
        "type" : { "type" : "string" },
        "title" : { "type" : "text" },
        "language" : { "type" : "string" },
        "field_keywords" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_isbn" : { "type" : "text" },
        "field_biblio_identifier" : { "type" : "text" },
        "field_server_identifier" : { "type" : "text" },
        "field_classification" : { "type" : "list\\u003Ctext\\u003E" },
        "field_other_titles" : { "type" : "list\\u003Ctext\\u003E" },
        "field_language" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_document_type" : { "type" : "integer", "entity_type" : "taxonomy_term" },
        "field_part_title" : { "type" : "list\\u003Ctext\\u003E" },
        "field_authors" : { "type" : "text" },
        "field_edition" : { "type" : "text" },
        "field_publisher" : { "type" : "text" },
        "field_pubdate" : { "type" : "text" },
        "field_coded_pubdate" : { "type" : "integer" },
        "field_series" : { "type" : "list\\u003Ctext\\u003E" },
        "field_edition_samemedia" : { "type" : "list\\u003Ctext\\u003E" },
        "field_edition_othermedia" : { "type" : "list\\u003Ctext\\u003E" },
        "field_relatedwork" : { "type" : "list\\u003Ctext\\u003E" },
        "field_level_set" : { "type" : "list\\u003Ctext\\u003E" },
        "field_level_piece" : { "type" : "list\\u003Ctext\\u003E" },
        "field_level_analytic" : { "type" : "list\\u003Ctext\\u003E" },
        "field_variant_title" : { "type" : "list\\u003Ctext\\u003E" },
        "field_ex_itype" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_ex_homebranch" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_ex_location" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_new" : { "type" : "list\\u003Cinteger\\u003E", "entity_type" : "taxonomy_term" },
        "field_700_main_txt" : { "type" : "text" },
        "field_701_main_txt" : { "type" : "list\\u003Ctext\\u003E" },
        "field_702_secondary_txt" : { "type" : "list\\u003Ctext\\u003E" },
        "field_710_main_txt" : { "type" : "text" },
        "field_711_main_txt" : { "type" : "list\\u003Ctext\\u003E" },
        "field_712_secondary_txt" : { "type" : "list\\u003Ctext\\u003E" },
        "field_all_subjects" : { "type" : "list\\u003Ctext\\u003E" },
        "field_series_title_txt" : { "type" : "list\\u003Ctext\\u003E" },
        "field_personal_name_txt" : { "type" : "list\\u003Ctext\\u003E" },
        "field_corporate_name_txt" : { "type" : "list\\u003Ctext\\u003E" },
        "field_family_name_txt" : { "type" : "list\\u003Ctext\\u003E" },
        "field_subject_txt" : { "type" : "list\\u003Ctext\\u003E" },
        "field_geo_subject_txt" : { "type" : "list\\u003Ctext\\u003E" },
        "field_uncont_subject_txt" : { "type" : "list\\u003Ctext\\u003E" },
        "field_issn" : { "type" : "text" },
        "search_api_language" : { "type" : "string" },
        "search_api_multi_aggregation_1" : { "type" : "list\\u003Ctext\\u003E" },
        "search_api_multi_aggregation_2" : { "type" : "list\\u003Ctext\\u003E" },
        "search_api_aggregation_1" : { "type" : "text" },
        "search_api_aggregation_2" : { "type" : "text" },
        "search_api_aggregation_3" : { "type" : "string" },
        "search_api_aggregation_4" : { "type" : "string" },
        "field_notes:value" : { "type" : "list\\u003Ctext\\u003E" },
        "field_dissert_note:value" : { "type" : "text" },
        "field_subject_id:field_see" : { "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E" },
        "field_geo_subject_id:field_see" : { "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E" },
        "field_700_main_id:field_see" : { "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E" },
        "field_701_main_id:field_see" : { "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E" },
        "field_710_main_id:field_see" : { "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E" },
        "field_711_main_id:field_see" : { "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E" },
        "field_702_secondary_id:field_see" : { "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E" },
        "field_712_secondary_id:field_see" : { "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E" },
        "field_all_subjects_id:field_see" : { "type" : "list\\u003Clist\\u003Ctext\\u003E\\u003E" }
      },
      "data_alter_callbacks" : {
        "search_api_alter_bundle_filter" : {
          "status" : 0,
          "weight" : "-10",
          "settings" : { "default" : "1", "bundles" : [] }
        },
        "search_api_alter_language_control" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "lang_field" : "", "languages" : [] }
        },
        "search_api_alter_node_access" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_node_status" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_hierarchy" : { "status" : 0, "weight" : "0", "settings" : { "fields" : [] } },
        "search_api_alter_add_viewed_entity" : { "status" : 0, "weight" : "0", "settings" : { "mode" : "full" } },
        "search_api_alter_add_url" : { "status" : 0, "weight" : "0", "settings" : [] },
        "search_api_alter_add_aggregation" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "search_api_aggregation_1" : {
                "name" : "Tous sujets",
                "type" : "fulltext",
                "fields" : [
                  "field_keywords",
                  "field_subject_id",
                  "field_geo_subject_id",
                  "field_all_subjects",
                  "field_all_subjects_id",
                  "field_personal_name_txt",
                  "field_corporate_name_txt",
                  "field_family_name_txt",
                  "field_subject_txt",
                  "field_geo_subject_txt",
                  "field_uncont_subject_txt",
                  "field_subject_id:field_see",
                  "field_geo_subject_id:field_see",
                  "field_all_subjects_id:field_see"
                ],
                "description" : "A Fulltext aggregation of the following fields: Mots cl\\u00e9, Sujet (autorit\\u00e9), Sujet g\\u00e9ographique (autorit\\u00e9), Sujets, Sujets (autorit\\u00e9), Sujet nom de personne, Sujet collectivit\\u00e9, Sujet nom de famille, Sujet nom commun, Sujet g\\u00e9ographique, Sujet libre, Sujet (autorit\\u00e9) \\u00bb Forme rejet\\u00e9e, Sujet g\\u00e9ographique (autorit\\u00e9) \\u00bb Forme rejet\\u00e9e, Sujets (autorit\\u00e9) \\u00bb Forme rejet\\u00e9e."
              },
              "search_api_aggregation_2" : {
                "name" : "Tous auteurs",
                "type" : "fulltext",
                "fields" : [
                  "field_authors",
                  "field_700_main_id",
                  "field_701_main_id",
                  "field_710_main_id",
                  "field_711_main_id",
                  "field_702_secondary_id",
                  "field_712_secondary_id",
                  "field_700_main_txt",
                  "field_701_main_txt",
                  "field_702_secondary_txt",
                  "field_710_main_txt",
                  "field_711_main_txt",
                  "field_712_secondary_txt",
                  "field_700_main_id:field_see",
                  "field_701_main_id:field_see",
                  "field_710_main_id:field_see",
                  "field_711_main_id:field_see",
                  "field_702_secondary_id:field_see",
                  "field_712_secondary_id:field_see"
                ],
                "description" : "A Fulltext aggregation of the following fields: Auteurs, Auteur principal (autorit\\u00e9), Co-auteur (autorit\\u00e9), Auteur principal collectif (autorit\\u00e9), Co-auteur collectif (autorit\\u00e9), Auteur secondaire (autorit\\u00e9), Auteur collectif secondaire (autorit\\u00e9), Auteur principal, Co-auteur, Auteur secondaire, Auteur collectif, Co-auteur collectif, Auteur collectif secondaire, Auteur principal (autorit\\u00e9) \\u00bb Forme rejet\\u00e9e, Co-auteur (autorit\\u00e9) \\u00bb Forme rejet\\u00e9e, Auteur principal collectif (autorit\\u00e9) \\u00bb Forme rejet\\u00e9e, Co-auteur collectif (autorit\\u00e9) \\u00bb Forme rejet\\u00e9e, Auteur secondaire (autorit\\u00e9) \\u00bb Forme rejet\\u00e9e, Auteur collectif secondaire (autorit\\u00e9) \\u00bb Forme rejet\\u00e9e."
              },
              "search_api_aggregation_3" : {
                "name" : "Titre_tri",
                "type" : "fulltext",
                "fields" : [ "title" ],
                "description" : "A Fulltext aggregation of the following fields: Titre."
              },
              "search_api_aggregation_4" : {
                "name" : "Auteur_tri",
                "type" : "fulltext",
                "fields" : [ "field_700_main_txt", "field_710_main_txt" ],
                "description" : "A Fulltext aggregation of the following fields: Auteur principal, Auteur collectif."
              }
            }
          }
        },
        "search_api_alter_add_multi_aggregation" : {
          "status" : 1,
          "weight" : "0",
          "settings" : { "fields" : {
              "search_api_multi_aggregation_1" : {
                "name" : "Facettes_auteur",
                "fields" : [
                  "field_700_main_txt",
                  "field_701_main_txt",
                  "field_702_secondary_txt",
                  "field_710_main_txt",
                  "field_711_main_txt",
                  "field_712_secondary_txt"
                ],
                "description" : "A text aggregation of the following fields: Auteur principal, Co-auteur, Auteur secondaire, Auteur collectif, Co-auteur collectif, Auteur collectif secondaire."
              },
              "search_api_multi_aggregation_2" : {
                "name" : "Facettes_sujets",
                "fields" : [ "field_all_subjects" ],
                "description" : "A text aggregation of the following fields: Sujets."
              }
            }
          }
        }
      },
      "processors" : {
        "search_api_case_ignore" : {
          "status" : 0,
          "weight" : "0",
          "settings" : { "fields" : {
              "title" : true,
              "field_isbn" : true,
              "field_biblio_identifier" : true,
              "field_server_identifier" : true,
              "field_classification" : true,
              "field_other_titles" : true,
              "field_part_title" : true,
              "field_edition" : true,
              "field_publisher" : true,
              "field_pubdate" : true,
              "field_series" : true,
              "field_edition_samemedia" : true,
              "field_edition_othermedia" : true,
              "field_relatedwork" : true,
              "field_level_set" : true,
              "field_level_piece" : true,
              "field_level_analytic" : true,
              "field_variant_title" : true,
              "field_issn" : true
            }
          }
        },
        "search_api_html_filter" : {
          "status" : 0,
          "weight" : "10",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_isbn" : true,
              "field_biblio_identifier" : true,
              "field_server_identifier" : true,
              "field_classification" : true,
              "field_other_titles" : true,
              "field_part_title" : true,
              "field_edition" : true,
              "field_publisher" : true,
              "field_pubdate" : true,
              "field_series" : true,
              "field_edition_samemedia" : true,
              "field_edition_othermedia" : true,
              "field_relatedwork" : true,
              "field_level_set" : true,
              "field_level_piece" : true,
              "field_level_analytic" : true,
              "field_variant_title" : true,
              "field_issn" : true
            },
            "title" : 0,
            "alt" : 1,
            "tags" : "h1 = 5\\r\\nh2 = 3\\r\\nh3 = 2\\r\\nstrong = 2\\r\\nb = 2\\r\\nem = 1.5\\r\\nu = 1.5"
          }
        },
        "search_api_tokenizer" : {
          "status" : 0,
          "weight" : "20",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_isbn" : true,
              "field_biblio_identifier" : true,
              "field_server_identifier" : true,
              "field_classification" : true,
              "field_other_titles" : true,
              "field_part_title" : true,
              "field_edition" : true,
              "field_publisher" : true,
              "field_pubdate" : true,
              "field_series" : true,
              "field_edition_samemedia" : true,
              "field_edition_othermedia" : true,
              "field_relatedwork" : true,
              "field_level_set" : true,
              "field_level_piece" : true,
              "field_level_analytic" : true,
              "field_variant_title" : true,
              "field_issn" : true
            },
            "spaces" : "[^[:alnum:]]",
            "ignorable" : "[\\u0027]"
          }
        },
        "search_api_stopwords" : {
          "status" : 0,
          "weight" : "30",
          "settings" : {
            "fields" : {
              "title" : true,
              "field_isbn" : true,
              "field_biblio_identifier" : true,
              "field_server_identifier" : true,
              "field_classification" : true,
              "field_other_titles" : true,
              "field_part_title" : true,
              "field_edition" : true,
              "field_publisher" : true,
              "field_pubdate" : true,
              "field_series" : true,
              "field_edition_samemedia" : true,
              "field_edition_othermedia" : true,
              "field_relatedwork" : true,
              "field_level_set" : true,
              "field_level_piece" : true,
              "field_level_analytic" : true,
              "field_variant_title" : true,
              "field_issn" : true
            },
            "file" : "",
            "stopwords" : "but\\r\\ndid\\r\\nthe this that those\\r\\netc"
          }
        }
      }
    },
    "enabled" : "1",
    "read_only" : "0"
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function wheke_solr_indexes_default_search_api_server() {
  $items = array();
  $items['solr_local'] = entity_import('search_api_server', '{
    "name" : "solr_local",
    "machine_name" : "solr_local",
    "description" : "",
    "class" : "search_api_solr_service",
    "options" : {
      "host" : "localhost",
      "port" : "8983",
      "path" : "\\/solr",
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0
    },
    "enabled" : "1"
  }');
  return $items;
}
