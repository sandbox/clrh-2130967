<?php
/**
 * @file
 * wheke_ct_opac_mapping.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wheke_ct_opac_mapping_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_opac_features_default_opac_content_type().
 */
function wheke_ct_opac_mapping_opac_features_default_opac_content_type() {
$opac_types = array();
  $opac_types[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'matching_field' => 'type_doc',
  'value' => '/.*/',
  'weight' => '0',
);
  $opac_types[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'matching_field' => 'type_doc',
  'value' => '/^PER$/',
  'weight' => '0',
);
return $opac_types;
}

/**
 * Implements hook_opac_items_features_default_mapping().
 */
function wheke_ct_opac_mapping_opac_items_features_default_mapping() {
$mapping = array();
  $mapping[] = array(
  'serv_id' => 'opacserver',
  'item_field' => 'date_due',
  'display_header' => 'Retour le',
  'vocabulary_machine_name' => NULL,
  'vocabulary_name' => NULL,
  'term_field' => NULL,
  'term_field_label' => NULL,
  'nomatch_rule' => NULL,
  'show_as_link' => 0,
  'weight' => '-5',
);
  $mapping[] = array(
  'serv_id' => 'opacserver',
  'item_field' => 'holdingbranch',
  'display_header' => 'Bibliothèque',
  'vocabulary_machine_name' => 'library',
  'vocabulary_name' => 'Bibliothèques',
  'term_field' => 'field_code',
  'term_field_label' => 'Code',
  'nomatch_rule' => 'none',
  'show_as_link' => 1,
  'weight' => '-10',
);
  $mapping[] = array(
  'serv_id' => 'opacserver',
  'item_field' => 'itemcallnumber',
  'display_header' => 'Cote',
  'vocabulary_machine_name' => NULL,
  'vocabulary_name' => NULL,
  'term_field' => NULL,
  'term_field_label' => NULL,
  'nomatch_rule' => NULL,
  'show_as_link' => 0,
  'weight' => '-7',
);
  $mapping[] = array(
  'serv_id' => 'opacserver',
  'item_field' => 'itype',
  'display_header' => 'Type de prêt',
  'vocabulary_machine_name' => 'itype',
  'vocabulary_name' => 'Itype',
  'term_field' => 'field_code',
  'term_field_label' => 'Code',
  'nomatch_rule' => 'none',
  'show_as_link' => 1,
  'weight' => '-8',
);
  $mapping[] = array(
  'serv_id' => 'opacserver',
  'item_field' => 'location',
  'display_header' => 'Localisation',
  'vocabulary_machine_name' => 'location',
  'vocabulary_name' => 'Localisations',
  'term_field' => 'field_code',
  'term_field_label' => 'Code',
  'nomatch_rule' => 'none',
  'show_as_link' => 1,
  'weight' => '-9',
);
  $mapping[] = array(
  'serv_id' => 'opacserver',
  'item_field' => 'notforloan',
  'display_header' => 'Disponibilité',
  'vocabulary_machine_name' => 'statut_exemplaire',
  'vocabulary_name' => 'Statut exemplaire',
  'term_field' => 'field_code',
  'term_field_label' => 'Code',
  'nomatch_rule' => 'none',
  'show_as_link' => 1,
  'weight' => '-6',
);
return $mapping;
}

/**
 * Implements hook_opac_features_default_opac_mapping().
 */
function wheke_ct_opac_mapping_opac_features_default_opac_mapping() {
$opac_mappings = array();
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_700_main_id',
  'node_field_label' => 'Auteur principal (autorité)',
  'mapped_with' => '700_responsibility_ids',
  'plugin' => 'authority_link',
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_700_main_txt',
  'node_field_label' => 'Auteur principal',
  'mapped_with' => '700_responsibility',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_701_main_id',
  'node_field_label' => 'Co-auteur (autorité)',
  'mapped_with' => '701_responsibility_ids',
  'plugin' => 'authority_link',
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_701_main_txt',
  'node_field_label' => 'Co-auteur',
  'mapped_with' => '701_responsibility',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_702_secondary_id',
  'node_field_label' => 'Auteur secondaire (autorité)',
  'mapped_with' => '702_responsibility_ids',
  'plugin' => 'authority_link',
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_702_secondary_txt',
  'node_field_label' => 'Auteur secondaire',
  'mapped_with' => '702_responsibility',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_710_main_id',
  'node_field_label' => 'Auteur principal collectif (autorité)',
  'mapped_with' => '710_responsibility_ids',
  'plugin' => 'authority_link',
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_710_main_txt',
  'node_field_label' => 'Auteur collectif',
  'mapped_with' => '710_responsibility',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_711_main_id',
  'node_field_label' => 'Co-auteur collectif (autorité)',
  'mapped_with' => '711_responsibility_ids',
  'plugin' => 'authority_link',
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_711_main_txt',
  'node_field_label' => 'Co-auteur collectif',
  'mapped_with' => '711_responsibility',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_712_secondary_id',
  'node_field_label' => 'Auteur collectif secondaire (autorité)',
  'mapped_with' => '712_responsibility_ids',
  'plugin' => 'authority_link',
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_712_secondary_txt',
  'node_field_label' => 'Auteur collectif secondaire',
  'mapped_with' => '712_responsibility',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_all_subjects',
  'node_field_label' => 'Sujets',
  'mapped_with' => 'all_subjects',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_all_subjects_id',
  'node_field_label' => 'Sujets (autorité)',
  'mapped_with' => 'all_subjects_ids',
  'plugin' => 'authority_link',
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_authors',
  'node_field_label' => 'Auteurs',
  'mapped_with' => '200_authors',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_classification',
  'node_field_label' => 'Classification',
  'mapped_with' => 'classification',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_coded_pubdate',
  'node_field_label' => 'Date de publication (date)',
  'mapped_with' => 'publicationdate',
  'plugin' => 'integer',
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_content_note',
  'node_field_label' => 'Note de contenu',
  'mapped_with' => 'contents_note',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_corporate_name_txt',
  'node_field_label' => 'Sujet collectivité',
  'mapped_with' => 'corporate_subjects',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_dissert_note',
  'node_field_label' => 'Note de thèse',
  'mapped_with' => 'dissertation_note',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_document_type',
  'node_field_label' => 'Type de document',
  'mapped_with' => 'type_doc',
  'plugin' => 'taxonomy',
  'plugin_options' => 'a:6:{s:23:"vocabulary_machine_name";s:13:"document_type";s:3:"vid";s:1:"2";s:15:"vocabulary_name";s:17:"Types de document";s:16:"term_field_label";s:4:"Code";s:10:"term_field";s:10:"field_code";s:12:"nomatch_rule";s:4:"none";}',
  'vocabulary_machine_name' => 'document_type',
  'vid' => '2',
  'vocabulary_name' => 'Types de document',
  'term_field_label' => 'Code',
  'term_field' => 'field_code',
  'nomatch_rule' => 'none',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_edition',
  'node_field_label' => 'Mention d\'édition',
  'mapped_with' => 'edition',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_edition_othermedia',
  'node_field_label' => 'Autre édition sur autre support',
  'mapped_with' => 'relation_452',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_edition_samemedia',
  'node_field_label' => 'Autre édition sur un même support',
  'mapped_with' => 'relation_451',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_ex_homebranch',
  'node_field_label' => 'Exemplaire homebranch',
  'mapped_with' => 'homebranch',
  'plugin' => 'taxonomy',
  'plugin_options' => 'a:6:{s:23:"vocabulary_machine_name";s:7:"library";s:3:"vid";s:1:"9";s:15:"vocabulary_name";s:14:"Bibliothèques";s:16:"term_field_label";s:4:"Code";s:10:"term_field";s:10:"field_code";s:12:"nomatch_rule";s:4:"none";}',
  'vocabulary_machine_name' => 'library',
  'vid' => '9',
  'vocabulary_name' => 'Bibliothèques',
  'term_field_label' => 'Code',
  'term_field' => 'field_code',
  'nomatch_rule' => 'none',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_ex_itype',
  'node_field_label' => 'Exemplaire itype',
  'mapped_with' => 'itype',
  'plugin' => 'taxonomy',
  'plugin_options' => 'a:2:{s:10:"term_field";s:10:"field_code";s:12:"nomatch_rule";s:4:"none";}',
  'term_field' => 'field_code',
  'nomatch_rule' => 'none',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_ex_location',
  'node_field_label' => 'Exemplaire localisation',
  'mapped_with' => 'location',
  'plugin' => 'taxonomy',
  'plugin_options' => 'a:6:{s:23:"vocabulary_machine_name";s:8:"location";s:3:"vid";s:2:"10";s:15:"vocabulary_name";s:13:"Localisations";s:16:"term_field_label";s:4:"Code";s:10:"term_field";s:10:"field_code";s:12:"nomatch_rule";s:4:"none";}',
  'vocabulary_machine_name' => 'location',
  'vid' => '10',
  'vocabulary_name' => 'Localisations',
  'term_field_label' => 'Code',
  'term_field' => 'field_code',
  'nomatch_rule' => 'none',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_family_name_txt',
  'node_field_label' => 'Sujet nom de famille',
  'mapped_with' => 'family_subjects',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_geo_subject_id',
  'node_field_label' => 'Sujet géographique (autorité)',
  'mapped_with' => 'geo_subjects_ids',
  'plugin' => 'authority_link',
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_geo_subject_txt',
  'node_field_label' => 'Sujet géographique',
  'mapped_with' => 'geo_subjects',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_isbn',
  'node_field_label' => 'ISBN',
  'mapped_with' => 'isbn',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_language',
  'node_field_label' => 'Langue du document',
  'mapped_with' => 'language',
  'plugin' => 'taxonomy',
  'plugin_options' => 'a:6:{s:23:"vocabulary_machine_name";s:8:"language";s:3:"vid";s:1:"3";s:15:"vocabulary_name";s:7:"Langues";s:16:"term_field_label";s:4:"Code";s:10:"term_field";s:10:"field_code";s:12:"nomatch_rule";s:4:"none";}',
  'vocabulary_machine_name' => 'language',
  'vid' => '3',
  'vocabulary_name' => 'Langues',
  'term_field_label' => 'Code',
  'term_field' => 'field_code',
  'nomatch_rule' => 'none',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_level_analytic',
  'node_field_label' => 'Niveau du dépouillement',
  'mapped_with' => 'level_piece_analytic',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_level_piece',
  'node_field_label' => 'Niveau de l\'unité matérielle',
  'mapped_with' => 'level_piece',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_level_set',
  'node_field_label' => 'Niveau de l\'ensemble',
  'mapped_with' => 'level_set',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_notes',
  'node_field_label' => 'Notes',
  'mapped_with' => 'notes',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_other_titles',
  'node_field_label' => 'Autres titres',
  'mapped_with' => 'other_titles',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_part_title',
  'node_field_label' => 'Titre de partie',
  'mapped_with' => 'part_title',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_personal_name_txt',
  'node_field_label' => 'Sujet nom de personne',
  'mapped_with' => 'personal_subjects',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_physical_desc',
  'node_field_label' => 'Description matérielle',
  'mapped_with' => 'physical_description',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_pubdate',
  'node_field_label' => 'Date de publication',
  'mapped_with' => 'pub_date',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_publisher',
  'node_field_label' => 'Editeur',
  'mapped_with' => 'publication',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_relatedwork',
  'node_field_label' => 'Notice en relation',
  'mapped_with' => 'relation_488',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_series',
  'node_field_label' => 'Collection',
  'mapped_with' => 'series',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_series_title_txt',
  'node_field_label' => 'Titre de collection',
  'mapped_with' => 'series_title',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_subject_id',
  'node_field_label' => 'Sujet (autorité)',
  'mapped_with' => 'subjects_ids',
  'plugin' => 'authority_link',
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_subject_txt',
  'node_field_label' => 'Sujet nom commun',
  'mapped_with' => 'subjects',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_summary',
  'node_field_label' => 'Résumé',
  'mapped_with' => 'summary',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_uncont_subject_txt',
  'node_field_label' => 'Sujet libre',
  'mapped_with' => 'free_subjects',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_uri',
  'node_field_label' => 'URI',
  'mapped_with' => 'elec_loc',
  'plugin' => 'url',
  'plugin_options' => 'a:3:{s:10:"url_string";s:6:"!value";s:9:"url_title";s:6:"!value";s:10:"field_type";s:10:"link_field";}',
  'url_string' => '!value',
  'url_title' => '!value',
  'field_type' => 'link_field',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'biblio',
  'node_field_name' => 'field_variant_title',
  'node_field_label' => 'Autre variante du titre',
  'mapped_with' => 'variant_title',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_authors',
  'node_field_label' => 'Auteurs',
  'mapped_with' => '200_authors',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_classification',
  'node_field_label' => 'Classification',
  'mapped_with' => 'classification',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_continuedby',
  'node_field_label' => 'Devient',
  'mapped_with' => 'relation_440',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_continues',
  'node_field_label' => 'Suite de',
  'mapped_with' => 'relation_430',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_document_type',
  'node_field_label' => 'Type de document',
  'mapped_with' => 'type_doc',
  'plugin' => 'taxonomy',
  'plugin_options' => 'a:6:{s:23:"vocabulary_machine_name";s:13:"document_type";s:3:"vid";s:1:"2";s:15:"vocabulary_name";s:17:"Types de document";s:16:"term_field_label";s:4:"Code";s:10:"term_field";s:10:"field_code";s:12:"nomatch_rule";s:4:"none";}',
  'vocabulary_machine_name' => 'document_type',
  'vid' => '2',
  'vocabulary_name' => 'Types de document',
  'term_field_label' => 'Code',
  'term_field' => 'field_code',
  'nomatch_rule' => 'none',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_edition',
  'node_field_label' => 'Mention d\'édition',
  'mapped_with' => 'edition',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_issn',
  'node_field_label' => 'ISSN',
  'mapped_with' => 'issn',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_language',
  'node_field_label' => 'Langue du document',
  'mapped_with' => 'language',
  'plugin' => 'taxonomy',
  'plugin_options' => 'a:6:{s:23:"vocabulary_machine_name";s:8:"language";s:3:"vid";s:1:"3";s:15:"vocabulary_name";s:7:"Langues";s:16:"term_field_label";s:4:"Code";s:10:"term_field";s:10:"field_code";s:12:"nomatch_rule";s:4:"none";}',
  'vocabulary_machine_name' => 'language',
  'vid' => '3',
  'vocabulary_name' => 'Langues',
  'term_field_label' => 'Code',
  'term_field' => 'field_code',
  'nomatch_rule' => 'none',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_notes',
  'node_field_label' => 'Notes',
  'mapped_with' => 'notes',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_other_titles',
  'node_field_label' => 'Autres titres',
  'mapped_with' => 'other_titles',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_pubdate',
  'node_field_label' => 'Date de publication',
  'mapped_with' => 'pub_date',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_publisher',
  'node_field_label' => 'Editeur',
  'mapped_with' => 'publication',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_serial',
  'node_field_label' => 'Etat de collection',
  'mapped_with' => 'serial',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_series',
  'node_field_label' => 'Collection',
  'mapped_with' => 'series',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_series_title_txt',
  'node_field_label' => 'Titre de collection',
  'mapped_with' => 'series_title',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_supersedby',
  'node_field_label' => 'Remplacé par',
  'mapped_with' => 'relation_442',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_supersedes',
  'node_field_label' => 'Remplace',
  'mapped_with' => 'relation_432',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_uri',
  'node_field_label' => 'URI',
  'mapped_with' => 'elec_loc',
  'plugin' => 'url',
  'plugin_options' => 'a:3:{s:10:"url_string";s:6:"!value";s:9:"url_title";s:6:"!value";s:10:"field_type";s:10:"link_field";}',
  'url_string' => '!value',
  'url_title' => '!value',
  'field_type' => 'link_field',
);
  $opac_mappings[] = array(
  'serv_id' => 'opacserver',
  'content_type' => 'revue',
  'node_field_name' => 'field_variant_title',
  'node_field_label' => 'Autre variante du titre',
  'mapped_with' => 'variant_title',
  'plugin' => NULL,
  'plugin_options' => 'a:0:{}',
);
return $opac_mappings;
}
