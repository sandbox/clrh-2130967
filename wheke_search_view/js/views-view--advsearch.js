(function ($) {

  $(document).ready(function() {
    // Group VBO "Select all" and "Operations"
    var select_all_fieldset = $('.view-advsearch fieldset.vbo-fieldset-select-all');
    var select_all_content = select_all_fieldset.find('.fieldset-wrapper > *');
    $('.view-advsearch fieldset#edit-select .fieldset-wrapper').prepend(select_all_content);
    select_all_fieldset.remove();

    results_count = $('div.view-advsearch div.view-content div.views-row').length;

    if (results_count > 0) {
      /* Copy the form and remove all but sort widgets and submit buttons. */
      var form = $('#views-exposed-form-advsearch-page').clone();
      form.find('.views-exposed-widget').not('.views-widget-sort-by,.views-widget-sort-order,.views-submit-button').each(function() {
        $(this).remove();
      });

      /* Add an hidden input for each parameter in url (except for sort parameters) */
      var params = get_url_parameters();
      for (i in params) {
        var name = params[i].name;
        var value = params[i].value;
        if (name != 'sort_bef_combine' && name != 'sort_by' && name != 'sort_order') {
          var $input = $('<input />')
            .attr('type', 'hidden')
            .attr('name', name)
            .val(value);
          form.append($input);
        }
      }

      form.find('.views-exposed-widget').css('float', 'left');
      /* (BEF) If sort-by and sort-order widgets are combined, the resulting
       * widget appear in inside the submit container... */
      form.find('.views-submit-button div.form-item-sort-bef-combine').css('display', 'inline');
      form.find('.views-submit-button .form-submit').css('margin-top', '0');
      $('.view-filters-replacement').append(form);
    }

    /* Click on the link to make the full form reappear. */
    $('#new-search-link').click(function() {
      show_exposed_form();
      return false;
    });

    /* Hide VBO if there is no result. */
    $('div.vbo-views-form').each(function() {
      if (results_count == 0) {
        $(this).find('fieldset#edit-select').hide();
      }
    });
  });

  function show_exposed_form() {
    $('.view-filters-replacement').hide();
    $('.view-filters').show();
  }

  function get_url_parameters() {
    var urlParams;
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    urlParams = [];
    while (match = search.exec(query)) {
      urlParams.push({
        name: decode(match[1]),
        value: decode(match[2])
      });
    }

    return urlParams;
  }

})(jQuery);
