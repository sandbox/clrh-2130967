<?php
/**
 * @file
 * wheke_user_opac_services_menu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function wheke_user_opac_services_menu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-panneau-de-services.
  $menus['menu-panneau-de-services'] = array(
    'menu_name' => 'menu-panneau-de-services',
    'title' => 'Panneau de services',
    'description' => '',
  );
  // Exported menu: user-menu.
  $menus['user-menu'] = array(
    'menu_name' => 'user-menu',
    'title' => 'User menu',
    'description' => 'Le menu <em>Utilisateur</em> contient les liens du compte utilisateur, comme le lien "Déconnexion"',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Le menu <em>Utilisateur</em> contient les liens du compte utilisateur, comme le lien "Déconnexion"');
  t('Panneau de services');
  t('User menu');


  return $menus;
}
