<?php
/**
 * @file
 * wheke_search_view.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wheke_search_view_views_api() {
  return array("version" => "3.0");
}
