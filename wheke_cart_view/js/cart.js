(function ($) {
  $(document).ready(function() {
    $(".view-header-cart p a").click(function() {
      if ($('.exportformat').css('display') == 'none') {
        $('.exportformat').show();
      }
      else {
        $('.exportformat').hide();
      }
      return false;
    });
  });
}(jQuery));
