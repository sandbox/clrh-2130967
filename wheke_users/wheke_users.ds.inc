<?php
/**
 * @file
 * wheke_users.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function wheke_users_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'user|user|default';
  $ds_layout->entity_type = 'user';
  $ds_layout->bundle = 'user';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_ufirstname',
        1 => 'field_uname',
        2 => 'opacserver:firstname',
        3 => 'opacserver:surname',
        4 => 'opacserver:address',
        5 => 'opacserver:address2',
        6 => 'opacserver:zipcode',
        7 => 'opacserver:city',
        8 => 'opacserver:mobile',
        9 => 'opacserver:email',
        10 => 'opacserver:emailpro',
      ),
      'right' => array(
        11 => 'opacserver:branchname',
        12 => 'opacserver:cardnumber',
        13 => 'opacserver:dateexpiry',
      ),
    ),
    'fields' => array(
      'field_ufirstname' => 'left',
      'field_uname' => 'left',
      'opacserver:firstname' => 'left',
      'opacserver:surname' => 'left',
      'opacserver:address' => 'left',
      'opacserver:address2' => 'left',
      'opacserver:zipcode' => 'left',
      'opacserver:city' => 'left',
      'opacserver:mobile' => 'left',
      'opacserver:email' => 'left',
      'opacserver:emailpro' => 'left',
      'opacserver:branchname' => 'right',
      'opacserver:cardnumber' => 'right',
      'opacserver:dateexpiry' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 0,
  );
  $export['user|user|default'] = $ds_layout;

  return $export;
}
