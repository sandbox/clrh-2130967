<?php
/**
 * @file
 * Template file for DS Layout 'wheke_layout3'.
 */
?>

<div class="<?php print $classes;?> clearfix">
  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <div class="wheke-layout3-container wheke-layout3-container-top">
    <div class="wheke-layout3-region wheke-layout3-region-top1 <?php print $top1_classes; ?>">
      <?php print $top1; ?>
    </div><!-- remove white space

    --><div class="wheke-layout3-container wheke-layout3-container-top-right">
      <div class="wheke-layout3-region wheke-layout3-region-top2 <?php print $top2_classes; ?>">
        <?php print $top2; ?>
      </div><!-- remove white space

      --><div class="wheke-layout3-region wheke-layout3-region-top3 <?php print $top3_classes; ?>">
        <?php print $top3; ?>
      </div><!-- remove white space

      --><div class="wheke-layout3-region wheke-layout3-region-top4 <?php print $top4_classes; ?>">
        <?php print $top4; ?>
      </div>
    </div>
  </div><!-- remove white space

  --><div class="wheke-layout3-region wheke-layout3-region-middle <?php print $middle_classes; ?>">
    <?php print $middle; ?>
  </div><!-- remove white space

  --><div class="wheke-layout3-container wheke-layout3-container-bottom">
    <div class="wheke-layout3-container wheke-layout3-container-bottom-left">
      <div class="wheke-layout3-region wheke-layout3-region-bottom1 <?php print $bottom1_classes; ?>">
        <?php print $bottom1; ?>
      </div><!-- remove white space

      --><div class="wheke-layout3-region wheke-layout3-region-bottom2 <?php print $bottom2_classes; ?>">
        <?php print $bottom2; ?>
      </div><!-- remove white space

      --><div class="wheke-layout3-region wheke-layout3-region-bottom3 <?php print $bottom3_classes; ?>">
        <?php print $bottom3; ?>
      </div>
    </div><!-- remove white space

    --><div class="wheke-layout3-region wheke-layout3-region-bottom4 <?php print $bottom4_classes; ?>">
        <?php print $bottom4; ?>
      </div>
    </div>
  </div>
</div>
