<?php
/**
 * @file
 * wheke_agenda.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function wheke_agenda_taxonomy_default_vocabularies() {
  return array(
    'event_type' => array(
      'name' => 'Type d\'événements',
      'machine_name' => 'event_type',
      'description' => 'Pour préciser le type d\'événement (animation, exposition ...)',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
    'public' => array(
      'name' => 'Public',
      'machine_name' => 'public',
      'description' => 'Pour préciser à qui s\'adresse le contenu, l\'événement, ...',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
    ),
  );
}
