<?php
/**
 * @file
 * Template file for DS Layout 'wheke_layout_2col_30_70'.
 */
?>

<div class="<?php print $classes;?> clearfix">
  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <div class="wheke-layout-2col-30-70-region wheke-layout-2col-30-70-region-left <?php print $left_classes; ?>">
    <?php print $left; ?>
  </div><!-- remove white space

  --><div class="wheke-layout-2col-30-70-region wheke-layout-2col-30-70-region-right">
    <?php print $right; ?>
  </div>

  <?php if (isset($bottom)): ?>
    <div class="wheke-layout-2col-30-70-region wheke-layout-2col-30-70-region-bottom <?php print $bottom_classes; ?>">
      <?php print $bottom; ?>
    </div>
  <?php endif; ?>
</div>
