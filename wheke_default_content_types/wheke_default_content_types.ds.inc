<?php
/**
 * @file
 * wheke_default_content_types.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function wheke_default_content_types_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|biblio|search_result';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'biblio';
  $ds_fieldsetting->view_mode = 'search_result';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h4',
        'class' => '',
      ),
    ),
  );
  $export['node|biblio|search_result'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|revue|search_result';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'revue';
  $ds_fieldsetting->view_mode = 'search_result';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h4',
        'class' => '',
      ),
    ),
  );
  $export['node|revue|search_result'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function wheke_default_content_types_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|biblio|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'biblio';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'wheke_layout1';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_document_type',
      ),
      'main_left' => array(
        1 => 'field_other_titles',
        2 => 'field_part_title',
        3 => 'field_authors',
        5 => 'field_isbn',
        6 => 'field_classification',
        7 => 'field_language',
        8 => 'field_edition',
        9 => 'field_publisher',
        10 => 'field_pubdate',
        11 => 'field_physical_desc',
        12 => 'field_series',
        17 => 'field_edition_samemedia',
        18 => 'field_edition_othermedia',
        19 => 'field_relatedwork',
        20 => 'field_level_set',
        21 => 'field_level_piece',
        22 => 'field_level_analytic',
        23 => 'field_variant_title',
        24 => 'field_uri',
        25 => 'field_700_main_txt',
        27 => 'field_701_main_txt',
        28 => 'field_702_secondary_txt',
        30 => 'field_710_main_txt',
        31 => 'field_711_main_txt',
        32 => 'field_712_secondary_txt',
        33 => 'field_all_subjects',
      ),
      'bottom' => array(
        4 => 'field_opac_items_info',
      ),
      'main_bottom' => array(
        13 => 'field_content_note',
        14 => 'field_notes',
        15 => 'field_dissert_note',
        16 => 'field_summary',
        26 => 'field_opac_items_avail',
      ),
      'main_right' => array(
        29 => 'field_opac_users_holdtitle',
      ),
    ),
    'fields' => array(
      'field_document_type' => 'left',
      'field_other_titles' => 'main_left',
      'field_part_title' => 'main_left',
      'field_authors' => 'main_left',
      'field_opac_items_info' => 'bottom',
      'field_isbn' => 'main_left',
      'field_classification' => 'main_left',
      'field_language' => 'main_left',
      'field_edition' => 'main_left',
      'field_publisher' => 'main_left',
      'field_pubdate' => 'main_left',
      'field_physical_desc' => 'main_left',
      'field_series' => 'main_left',
      'field_content_note' => 'main_bottom',
      'field_notes' => 'main_bottom',
      'field_dissert_note' => 'main_bottom',
      'field_summary' => 'main_bottom',
      'field_edition_samemedia' => 'main_left',
      'field_edition_othermedia' => 'main_left',
      'field_relatedwork' => 'main_left',
      'field_level_set' => 'main_left',
      'field_level_piece' => 'main_left',
      'field_level_analytic' => 'main_left',
      'field_variant_title' => 'main_left',
      'field_uri' => 'main_left',
      'field_700_main_txt' => 'main_left',
      'field_opac_items_avail' => 'main_bottom',
      'field_701_main_txt' => 'main_left',
      'field_702_secondary_txt' => 'main_left',
      'field_opac_users_holdtitle' => 'main_right',
      'field_710_main_txt' => 'main_left',
      'field_711_main_txt' => 'main_left',
      'field_712_secondary_txt' => 'main_left',
      'field_all_subjects' => 'main_left',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'main_left' => 'div',
      'main_right' => 'div',
      'main_bottom' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 0,
  );
  $export['node|biblio|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|biblio|search_result';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'biblio';
  $ds_layout->view_mode = 'search_result';
  $ds_layout->layout = 'wheke_layout1';
  $ds_layout->settings = array(
    'regions' => array(
      'main_left' => array(
        0 => 'title',
        2 => 'field_authors',
        3 => 'field_isbn',
        5 => 'field_edition',
        6 => 'field_publisher',
        7 => 'field_pubdate',
        8 => 'field_uri',
      ),
      'left' => array(
        1 => 'field_document_type',
      ),
      'bottom' => array(
        4 => 'field_opac_items_info',
      ),
      'main_right' => array(
        9 => 'field_opac_users_holdtitle',
      ),
      'main_bottom' => array(
        10 => 'field_opac_items_avail',
      ),
    ),
    'fields' => array(
      'title' => 'main_left',
      'field_document_type' => 'left',
      'field_authors' => 'main_left',
      'field_isbn' => 'main_left',
      'field_opac_items_info' => 'bottom',
      'field_edition' => 'main_left',
      'field_publisher' => 'main_left',
      'field_pubdate' => 'main_left',
      'field_uri' => 'main_left',
      'field_opac_users_holdtitle' => 'main_right',
      'field_opac_items_avail' => 'main_bottom',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'main_left' => 'div',
      'main_right' => 'div',
      'main_bottom' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 0,
  );
  $export['node|biblio|search_result'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|revue|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'revue';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'wheke_layout1';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_document_type',
      ),
      'main_left' => array(
        1 => 'field_issn',
        2 => 'field_language',
        4 => 'field_other_titles',
        5 => 'field_authors',
        6 => 'field_edition',
        7 => 'field_publisher',
        8 => 'field_pubdate',
        10 => 'field_continues',
        11 => 'field_supersedes',
        12 => 'field_continuedby',
        13 => 'field_supersedby',
        14 => 'field_level_set',
        15 => 'field_level_piece',
        16 => 'field_level_analytic',
        17 => 'field_classification',
        18 => 'field_uri',
        19 => 'field_series',
        21 => 'field_variant_title',
        22 => 'field_series_title_txt',
      ),
      'bottom' => array(
        3 => 'field_opac_items_info',
      ),
      'main_bottom' => array(
        9 => 'field_notes',
        20 => 'field_serial',
        23 => 'field_opac_items_avail',
      ),
      'main_right' => array(
        24 => 'field_opac_users_holdtitle',
      ),
    ),
    'fields' => array(
      'field_document_type' => 'left',
      'field_issn' => 'main_left',
      'field_language' => 'main_left',
      'field_opac_items_info' => 'bottom',
      'field_other_titles' => 'main_left',
      'field_authors' => 'main_left',
      'field_edition' => 'main_left',
      'field_publisher' => 'main_left',
      'field_pubdate' => 'main_left',
      'field_notes' => 'main_bottom',
      'field_continues' => 'main_left',
      'field_supersedes' => 'main_left',
      'field_continuedby' => 'main_left',
      'field_supersedby' => 'main_left',
      'field_level_set' => 'main_left',
      'field_level_piece' => 'main_left',
      'field_level_analytic' => 'main_left',
      'field_classification' => 'main_left',
      'field_uri' => 'main_left',
      'field_series' => 'main_left',
      'field_serial' => 'main_bottom',
      'field_variant_title' => 'main_left',
      'field_series_title_txt' => 'main_left',
      'field_opac_items_avail' => 'main_bottom',
      'field_opac_users_holdtitle' => 'main_right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'main_left' => 'div',
      'main_right' => 'div',
      'main_bottom' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 0,
  );
  $export['node|revue|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|revue|search_result';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'revue';
  $ds_layout->view_mode = 'search_result';
  $ds_layout->layout = 'wheke_layout1';
  $ds_layout->settings = array(
    'regions' => array(
      'main_left' => array(
        0 => 'title',
        1 => 'field_issn',
        4 => 'field_authors',
        5 => 'field_publisher',
        6 => 'field_pubdate',
        7 => 'field_uri',
      ),
      'bottom' => array(
        2 => 'field_opac_items_info',
      ),
      'left' => array(
        3 => 'field_document_type',
      ),
      'main_bottom' => array(
        8 => 'field_serial',
        9 => 'field_opac_items_avail',
      ),
      'main_right' => array(
        10 => 'field_opac_users_holdtitle',
      ),
    ),
    'fields' => array(
      'title' => 'main_left',
      'field_issn' => 'main_left',
      'field_opac_items_info' => 'bottom',
      'field_document_type' => 'left',
      'field_authors' => 'main_left',
      'field_publisher' => 'main_left',
      'field_pubdate' => 'main_left',
      'field_uri' => 'main_left',
      'field_serial' => 'main_bottom',
      'field_opac_items_avail' => 'main_bottom',
      'field_opac_users_holdtitle' => 'main_right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'main_left' => 'div',
      'main_right' => 'div',
      'main_bottom' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 0,
  );
  $export['node|revue|search_result'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'taxonomy_term|document_type|full';
  $ds_layout->entity_type = 'taxonomy_term';
  $ds_layout->bundle = 'document_type';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_icon',
      ),
    ),
    'fields' => array(
      'field_icon' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 0,
  );
  $export['taxonomy_term|document_type|full'] = $ds_layout;

  return $export;
}
