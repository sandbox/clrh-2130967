<?php
/**
 * @file
 * Template file for DS Layout 'ubside_layout_search_result'.
 */
?>

<div class="<?php print $classes;?> clearfix ubside-layout-search-result">
  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <div class="ubside-layout-search-result-content">
    <div class="ubside-layout-search-result-region ubside-layout-search-result-region-left <?php print $left_classes; ?>">
      <?php print $left; ?>
    </div>

    <div class="ubside-layout-search-result-region ubside-layout-search-result-region-right">
      <?php print $right; ?>
    </div>
  </div>

</div>
