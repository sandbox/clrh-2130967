<?php
/**
 * @file
 * wheke_agenda.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function wheke_agenda_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'calendrier_agenda';
  $context->description = 'Affiche le bloc calendrier dans la vue agenda';
  $context->tag = '';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'agenda:page' => 'agenda:page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-calendrier-block_1' => array(
          'module' => 'views',
          'delta' => 'calendrier-block_1',
          'region' => 'content_top',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;

  // Translatables
  // Included for use with string extractors like potx.
  t('Affiche le bloc calendrier dans la vue agenda');
  $export['calendrier_agenda'] = $context;

  return $export;
}
