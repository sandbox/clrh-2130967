<?php
/**
 * @file
 * Template file for DS Layout 'ubside_layout_full_record'.
 */
?>

<div class="<?php print $classes;?> clearfix ubside-layout-full-record">
  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <div class="ubside-layout-full-record-content">
    <div class="ubside-layout-full-record-region ubside-layout-full-record-region-left <?php print $left_classes; ?>">
      <?php print $left; ?>
    </div>

    <div class="ubside-layout-full-record-region ubside-layout-full-record-main-wrapper">
      <div class="ubside-layout-full-record-region ubside-layout-full-record-region-main">
        <?php if ($right): ?>
          <div class="ubside-layout-full-record-region ubside-layout-full-record-region-right">
            <?php print $right; ?>
          </div>
        <?php endif; ?>
        <?php print $main; ?>
      </div>
      <?php if ($bottom): ?>
        <div class="ubside-layout-full-record-region ubside-layout-full-record-region-bottom">
          <?php print $bottom ?>
        </div>
      <?php endif; ?>
    </div>
  </div>

</div>
