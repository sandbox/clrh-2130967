<?php
/**
 * @file
 * wheke_default_authority_types.features.inc
 */

/**
 * Implements hook_default_opac_authority_type().
 */
function wheke_default_authority_types_default_opac_authority_type() {
  $items = array();
  $items['corporate_name'] = entity_import('opac_authority_type', '{
    "type" : null,
    "label" : "Collectivit\\u00e9",
    "weight" : 0,
    "authtype" : "corporate_name",
    "description" : ""
  }');
  $items['personal_name'] = entity_import('opac_authority_type', '{
    "type" : null,
    "label" : "Nom de personne",
    "weight" : 0,
    "authtype" : "personal_name",
    "description" : "Auteur, ou sujet nom de personne"
  }');
  $items['subject'] = entity_import('opac_authority_type', '{
    "type" : null,
    "label" : "Sujet nom commun",
    "weight" : 0,
    "authtype" : "subject",
    "description" : ""
  }');
  $items['subject_geo'] = entity_import('opac_authority_type', '{
    "type" : null,
    "label" : "Sujet g\\u00e9ographique",
    "weight" : 0,
    "authtype" : "subject_geo",
    "description" : ""
  }');
  return $items;
}
